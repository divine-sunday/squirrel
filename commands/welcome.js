let global = require("./../global.js");

module.exports = {


    execute(msg, args){

        if(args.length > 0){

            if(args[0] == "define"){

                let welcomeMsg = "";

                for(let i = 1; i < args.length; i++){
                    if(i == 1){
                        welcomeMsg = args[i];
                    }else{ 
                        welcomeMsg += " "  + args[i];
                    }
                }

                msg.channel.send("***Nouveau message de bienvenue:*** " + welcomeMsg);

                //add to global
                global.welcomeMsg = welcomeMsg;

            }else if(args[0] == "show"){
                //show welcomeMsg
                msg.channel.send("***Message de bienvenue actuel: ***" + global.welcomeMsg);
            }

        }else{
            msg.channel.send(":x: Mauvaise syntaxe, veuillez essayer: !welcome <message de bienvenue>");
        }

    }

}