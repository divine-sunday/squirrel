let global = require("./../global.js");
module.exports = {

    execute(msg, args) {


        if(!msg.member.hasPermission("MANAGE_MESSAGES", false, true, true)) return;


        if(args.length == 0){
            //Activate/Desactivate noTalk mode
            if(global.noTalk == false){
                global.noTalk = true;
                msg.channel.send(":no_entry: ***Plus personne n'est autorisé à parler***");
            }else{
                global.noTalk = false;
                msg.channel.send(":mega: ***Vous pouvez de nouveau parler***");
            }
        }else if(args.length == 2){

            if(args[0] == "add"){

                console.log(msg.mentions.users.first().username);


                if(!global.noTalkByPass.includes(msg.mentions.users.first().username)){
                    global.noTalkByPass.push(msg.mentions.users.first().username);
                    msg.channel.send(":v: ***" + msg.mentions.users.first() + " a maintenant accès au byPass du noTalk***");
                    
                }else{
                    msg.channel.send(":interrobang: ***Ce membre byPass déjà le noTalk***");
                }

            }else if(args[0] == "remove"){

                if(global.noTalkByPass.includes(msg.mentions.users.first().username)){
                    
                    //NOT WORKING AT THE MOMENT
                    console.log("avant: " + global.noTalkByPass);
                    console.log("index: " + global.noTalkByPass.indexOf(msg.mentions.users.first().username));
                    global.noTalkByPass.slice(0, 1);

                    msg.channel.send(":v: ***" + msg.mentions.users.first() + " n'as plus accès au byPass du noTalk***");
                    console.log("apres: " + global.noTalkByPass);
                }else{
                    msg.channel.send(":interrobang: ***Ce membre ne byPass pas le noTalk***");
                }

            }else{
                msg.channel.send("Mauvaise syntaxe: essayez !NoTalk <add/remove> <membre>");
            }

        }

    }

}