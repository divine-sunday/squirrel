module.exports = {

    execute(msg, args){

        if(!msg.member.hasPermission("KICK_MEMBERS", false, true, true));


        if(args.length == 1){

            let target = msg.mentions.members.first();

            if(!target.bot){

                if(msg.member.highestRole.comparePositionTo(target.highestRole)){

                    target.kick();

                    msg.channel.send(":door: " + target + " a été kick par " + msg.author);

                }else{
                    msg.channel.send(":x: Le membre ciblé est hiérarchiquement plus haut placé que vous");
                }

            }else{
                msg.channel.send(":x: Vous ne pouvez pas kick un bot");
            }

        }else{
            msg.channel.send(":x: Mauvaise syntaxe, essayez plutôt: !kick <membre>");
        }

    }

}