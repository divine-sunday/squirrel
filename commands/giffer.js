let giphy = require("giphy-api")();

module.exports = {

    execute(msg, args){

        if(args.length == 1){

            let gifTheme = args[0];

            


            giphy.search({q: gifTheme, limit: "99"}, (err, res) =>{
                console.log(res);
                if(res){
                    console.log(res.data.length);
                    let r = Math.floor(Math.random() * res.data.length);

                    msg.channel.send(res.data[r].url);
                }else{
                    msg.channel.send(":x: Pas de résultat");
                }

            });

        }else{
            msg.channel.send(":x: Mauvaise syntaxe, essayez plutôt: !giffer <gif theme>");
        }

    }


}