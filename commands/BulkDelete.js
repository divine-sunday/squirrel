module.exports = {

    execute(msg, args){

        if(msg.member.hasPermission("MANAGE_MESSAGES", false, true, true)){

            if(args.length == 1){
                msg.channel.bulkDelete(Number(args[0])+1);

                msg.channel.send(":scissors: ***" + args[0] + " messages ont été supprimés par " + msg.author.username + "***");

            }else{
                msg.channel.send("Mauvaise syntaxe: essayez !bulkDelete <nombre>");
            }

        }

    }

}