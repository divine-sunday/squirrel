let global = require("./../global.js");

module.exports = {

    execute(msg, args){


        if(!msg.member.hasPermission("MANAGE_MESSAGES", false, true, true));

        if(args.length == 1){

            let target = msg.mentions.members.first();

            if(msg.member.highestRole.comparePositionTo(target.highestRole)){

                //mute
                if(global.muted.includes(target.user.username)){

                    global.muted.slice(global.muted.indexOf(target.user.username), 1);
                    
                    msg.channel.send(":loud_sound:  " + target + " a été démute par " + msg.author);

                }else{
                    global.muted.push(target.username);
                    msg.channel.send(":mute: " + target + " a été mute par " + msg.author);
                }

                console.log(global.muted);

            }else{
                msg.channel.send(":x: Le membre ciblé est hiérarchiquement plus haut placé que vous");
            }

        }else{
            msg.channel.send(":x: Mauvaise syntaxe, essayez plutôt: !mute <membre>");
        }


    }

}