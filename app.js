const Discord = require("discord.js");
const client = new Discord.Client();
const fs = require("fs");

let config = require("./config.json");

client.commands = new Discord.Collection();


let noTalk = false;

fs.readdir("./commands", (err, files) =>{

    if(err) console.log(err);

    if(files.length == 0){

        console.log("Aucune commande trouvé"); 
        return;

    }

    let validFiles = files.filter(f => f.split(".").pop() == "js");

    validFiles.forEach(f =>{

        let commandName = f.split(".")[0].toLowerCase();
        let command = require("./commands/" + f);

        console.log("Commande trouvé: " + f.split(".")[0]);

        client.commands.set(commandName, command);

    });

});

client.on("message", (msg) => {
    if(msg.content == "Bloup"){
        msg.channel.send("Bloup wi Bloup");
    }

    // Check if it is a bot command
    if(msg.content.startsWith(config.prefix)){

        // Check author is a bot
        if(!msg.author.bot){

            let cmd = msg.content.split(" ");
            let commandName = cmd[0].slice(1).toLowerCase();
            let args = cmd.slice(1);

            // Execute the command

            if(client.commands.has(commandName)){

                client.commands.get(commandName).execute(msg, args);

            }else{
                msg.channel.send(":x: Cette commande n'est pas reconnue");
            }

        }

    }

});


fs.readdir("./events", (err, files) =>{

    if(err) console.log(err);

    let validEvents = files.filter(f => f.split(".").pop() === "js");
    
    

    validEvents.forEach(f => {

        let eventName = f.split(".")[0];
        let event = require("./events/" + f);

        client.on(eventName, event.bind(null, client));

    });
    
});



client.login(config.token);